import { TestBed, async, inject } from '@angular/core/testing';

import { StudentAndLecturerGuardGuard } from './student-and-lecturer-guard.guard';

describe('StudentAndLecturerGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentAndLecturerGuardGuard]
    });
  });

  it('should ...', inject([StudentAndLecturerGuardGuard], (guard: StudentAndLecturerGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
