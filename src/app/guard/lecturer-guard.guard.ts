import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../service/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class LecturerGuardGuard implements CanActivate {
  constructor(private router:Router,private authService:AuthenticationService){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state:RouterStateSnapshot): Observable<boolean> | Promise<boolean> |boolean {
    if (this.authService.hasRole('LECTURER')){
      return true;
    }
    this.router.navigate(['/login'],{queryParams: { returnUrl: state.url}});
    return false;
  }
  
}
