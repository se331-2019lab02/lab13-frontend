import { TestBed, async, inject } from '@angular/core/testing';

import { LecturerGuardGuard } from './lecturer-guard.guard';

describe('LecturerGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LecturerGuardGuard]
    });
  });

  it('should ...', inject([LecturerGuardGuard], (guard: LecturerGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
