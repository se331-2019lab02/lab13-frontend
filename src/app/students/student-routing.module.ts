import { Routes, RouterModule } from '@angular/router';
import { StudentsViewComponent } from './view/students.view.component';
import { StudentsAddComponent } from './add/students.add.component';
import { StudentsComponent } from './list/students.component';
import { NgModule } from '@angular/core';
import { StudentTableComponent } from './student-table/student-table.component';
import { LecturerGuardGuard } from '../guard/lecturer-guard.guard';
import { StudentAndLecturerGuardGuard } from '../guard/student-and-lecturer-guard.guard';



const StudentRoutes: Routes = [
    { path: 'add', component: StudentsAddComponent,canActivate:[LecturerGuardGuard] },
    { path: 'list', component: StudentTableComponent,canActivate:[StudentAndLecturerGuardGuard] },
    { path: 'detail/:id', component: StudentsViewComponent ,canActivate:[StudentAndLecturerGuardGuard] },
    { path: 'table', component: StudentTableComponent ,canActivate:[StudentAndLecturerGuardGuard] }
];
@NgModule({
    imports: [
        RouterModule.forRoot(StudentRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class StudentRoutingModule {

}
