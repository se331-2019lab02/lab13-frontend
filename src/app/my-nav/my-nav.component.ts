import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import Student from '../entity/student';
import { StudentService } from '../service/student-service';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css']
})
export class MyNavComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  get user(){
    return this.authService.getCurrentUser();
  }
  defaultImageUrl='assets/images/camt.jpg'; 
  constructor(private breakpointObserver: BreakpointObserver, private studentService: StudentService,private authService:AuthenticationService) {}
  students$: Observable<Student[]>  = this.studentService.getStudents();
  hasRole(role: string){
    return this.authService.hasRole(role);
  }
}
